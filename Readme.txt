Building and running the app:

Option 1:
After downloadin/cloning the repository, there is an option to build the application in Android studio. 
Either connect your phone via USB or create a virtual machine phone and run the application from
Android studio directly.
In case Android studio doesn't recognise your phone you need to install ADB drivers and enable developers mode.


Option 2:
In the map "app build" there is a file called app-debug.apk, which can be downloaded and installed on your phone.
After the installation it should run normally.


Assumptions:
Since it wasn't specified exactly how or where we need to keep our data, I have made a database on my personal
site. I have provided the PHP for the access to the database and other functionalities.
For "Public profile" it wasn't specified how it needs to work so I assumed that it should be made like a click on
a single employee. By clicking on the information about an employee you will get the "Public profile" search.

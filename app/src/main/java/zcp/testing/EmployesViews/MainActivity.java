package zcp.testing.EmployesViews;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import zcp.testing.EmployesViews.Fragments.AnalyticsFragment;
import zcp.testing.EmployesViews.Fragments.EmployeesFragment;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_CODE = 1;

    private int position = 2;

    @Override
    public void onCreate(Bundle appBundle){
        super.onCreate(appBundle);

        try{
            verifyPermissions();
        } catch (Exception e) {}

        setContentView(R.layout.main_layout);

        Button btnEmployees = (Button)findViewById(R.id.btnEmployees);
        btnEmployees.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                position = 0;
                fragmentChanger();
            }
        });
        Button btnAnalytics = (Button)findViewById(R.id.btnAnalytics);
        btnAnalytics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                position = 1;
                fragmentChanger();
            }
        });


    }

    private void fragmentChanger() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        ft.replace(R.id.fragment_holder, getProperFragment());

        ft.commit();
    }

    private Fragment getProperFragment() {
        switch (position) {
            case 0:
                return new EmployeesFragment();
            case 1:
                return new AnalyticsFragment();
        }

        return null;
    }



    private void verifyPermissions(){
        String[] permissions = {Manifest.permission.INTERNET};

        if(ContextCompat.checkSelfPermission(this.getApplicationContext(),
                permissions[0]) == PackageManager.PERMISSION_GRANTED){

        }else{
            ActivityCompat.requestPermissions(MainActivity.this,
                    permissions,
                    REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        verifyPermissions();
    }
}

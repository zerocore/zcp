package zcp.testing.EmployesViews;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Employee {

    private int id;
    private String name;
    private String birthDate;
    private String gender;
    private float salary;

    private LinearLayout view;

    public Employee(int id, String name, String birthDate, String gender, float salary) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.salary = salary;
    }

    public View getCustomView(final Context context, ViewGroup parent) {
        View customView = LayoutInflater.from(context).inflate(R.layout.single_employee_layout, parent, false);

        TextView txtName = (TextView)customView.findViewById(R.id.txtName);
        TextView txtBirthDay = (TextView)customView.findViewById(R.id.txtBirthDate);
        TextView txtGender = (TextView)customView.findViewById(R.id.txtGender);
        TextView txtSalary = (TextView)customView.findViewById(R.id.txtSalary);

        txtName.setText("Name: " + name);
        txtBirthDay.setText("Birth day: " + birthDate);
        txtGender.setText("Gender: " + gender);
        txtSalary.setText("Salary: " + salary+"$");

        customView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new HTMLHelper(context).execute(name);
            }
        });

        return customView;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    public LinearLayout getView() {
        return view;
    }
}

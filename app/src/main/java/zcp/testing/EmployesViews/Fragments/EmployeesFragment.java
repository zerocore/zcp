package zcp.testing.EmployesViews.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import java.util.ArrayList;

import zcp.testing.EmployesViews.DataClass;
import zcp.testing.EmployesViews.Employee;
import zcp.testing.EmployesViews.R;

public class EmployeesFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.employees_layout, parent, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        Button btnAdd = (Button) getActivity().findViewById(R.id.btnAddNewEmplo);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();

                ft.replace(R.id.fragment_holder, new AddEmploFragment());

                ft.commit();
            }
        });

        loadAllEmplos();
    }

    private void loadAllEmplos() {
        LinearLayout scroll = (LinearLayout) ((AppCompatActivity)getContext()).findViewById(R.id.emploScroll);

        ArrayList<Employee> employees = ((DataClass)getActivity().getApplication()).getAllEmployees();
        for(Employee e : employees) {
            scroll.addView(e.getCustomView(getContext(), scroll));
        }
    }
}

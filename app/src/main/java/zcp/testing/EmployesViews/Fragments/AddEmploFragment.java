package zcp.testing.EmployesViews.Fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

import zcp.testing.EmployesViews.DataClass;
import zcp.testing.EmployesViews.Employee;
import zcp.testing.EmployesViews.R;

public class AddEmploFragment extends Fragment {

    private Employee tempEmplo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.add_emplo_layout, parent, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        Button btnAdd = (Button) getActivity().findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText etName = (EditText) getActivity().findViewById(R.id.etName);
                EditText etDate = (EditText) getActivity().findViewById(R.id.etBirthday);
                Spinner spnGender = (Spinner) getActivity().findViewById(R.id.spnGender);
                EditText etSalary = (EditText) getActivity().findViewById(R.id.etSalary);

                tempEmplo = new Employee(0, etName.getText().toString(), etDate.getText().toString(), spnGender.getSelectedItem().toString(), Float.parseFloat(etSalary.getText().toString()));

                new AddEmplo().execute();
            }
        });
    }

    public class AddEmplo extends AsyncTask<String, Void, String> {

        protected void onPreExecute(){}

        protected String doInBackground(String... arg0) {
            try{

                URL url = new URL("https://lifeofshorties.eu/test/add_emplo.php");

                JSONObject postDataParams = new JSONObject();
                postDataParams.put("name", tempEmplo.getName());
                postDataParams.put("birthDay", tempEmplo.getBirthDate().toString());
                postDataParams.put("gender", tempEmplo.getGender());
                postDataParams.put("salary", tempEmplo.getSalary());
                postDataParams.put("appCode", "236sseOJ82aad");
                Log.e("params",postDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                }
                else {
                    return new String("Response: " + responseCode);
                }
            }
            catch(Exception e){
                return new String("Exception: " + e.getMessage());
            }

        }

        @Override
        protected void onPostExecute(String result) {
            ((DataClass)getActivity().getApplication()).addEmplo(tempEmplo);
            ((DataClass)getActivity().getApplication()).redownloadAnalysis();

            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();

            ft.replace(R.id.fragment_holder, new EmployeesFragment());

            ft.commit();
        }
    }

    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }
}

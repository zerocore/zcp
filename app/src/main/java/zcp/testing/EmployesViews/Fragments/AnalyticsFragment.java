package zcp.testing.EmployesViews.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import zcp.testing.EmployesViews.DataClass;
import zcp.testing.EmployesViews.R;

public class AnalyticsFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.analytics_layout, parent, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        TextView txtAvgAge = (TextView)(getActivity().findViewById(R.id.txtAverageAge));
        TextView txtRatio = (TextView)(getActivity().findViewById(R.id.txtRatio));
        TextView txtMedian = (TextView)(getActivity().findViewById(R.id.txtMedianAge));
        TextView txtMaxSalaray =(TextView)(getActivity().findViewById(R.id.txtMaxSalary));

        txtAvgAge.setText(((DataClass)getActivity().getApplication()).getAvgAge());
        txtMaxSalaray.setText(((DataClass)getActivity().getApplication()).getMaxSalary());
        txtMedian.setText(((DataClass)getActivity().getApplication()).getMedian());


        txtRatio.setText(1-((DataClass)getActivity().getApplication()).getRatio()+" male to "+ ((DataClass)getActivity().getApplication()).getRatio()+ " female");
    }
}

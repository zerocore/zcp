package zcp.testing.EmployesViews.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import zcp.testing.EmployesViews.DataClass;
import zcp.testing.EmployesViews.R;

public class GoogleHitsFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.public_profile_layout, parent, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        TextView txtView = (TextView) getActivity().findViewById(R.id.txtPublicHits);
        txtView.setText(((DataClass)getActivity().getApplication()).getHeaders());
    }
}

package zcp.testing.EmployesViews;

import android.app.Application;
import android.os.AsyncTask;

import androidx.fragment.app.FragmentTransaction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

import zcp.testing.EmployesViews.Fragments.EmployeesFragment;

public class DataClass extends Application {

    private ArrayList<Employee> array;
    private String headers;
    private String avgAge;
    private String median;
    private String maxSalary;
    private float ratio;


    public ArrayList<Employee> getAllEmployees() {
        return array;
    }

    public void addEmplo(Employee addable) {
        array.add(addable);
    }

    public void redownloadAnalysis() {
        new GetAnalytics().execute();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        array = new ArrayList<>();
        new GetAllEmployees().execute();
        new GetAnalytics().execute();
    }

    public String getAvgAge() {
        return avgAge;
    }

    public String getMedian() {
        return median;
    }

    public String getMaxSalary() {
        return maxSalary;
    }

    public float getRatio() {
        return ratio;
    }

    public String getHeaders() {
        return headers;
    }

    public void setHeaders(String headers) {
        this.headers = headers;
    }

    public class GetAllEmployees extends AsyncTask<String, Void, String> {

        protected void onPreExecute(){}

        protected String doInBackground(String... arg0) {
            try{

                URL url = new URL("https://lifeofshorties.eu/test/get_emplo.php");

                JSONObject postDataParams = new JSONObject();
                postDataParams.put("appCode", "236sseOJ82aad");

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                }
                else {
                    return new String("" + responseCode);
                }
            }
            catch(Exception e){
                return new String("Exception: " + e.getMessage());
            }

        }

        @Override
        protected void onPostExecute(String result) {
            parseEmployees(result);
        }
    }

    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }

    private void parseEmployees(String result) {
        try {
            JSONArray arr = new JSONObject(result).getJSONArray("data");

            for (int i=0; i < arr.length(); i++) {
                JSONObject temp = arr.getJSONObject(i);
                array.add(new Employee(temp.getInt("id"), temp.getString("name"), temp.getString("birth_date"), temp.getString("gender"), (float) temp.getDouble("salary")));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public class GetAnalytics extends AsyncTask<String, Void, String> {

        protected void onPreExecute(){}

        protected String doInBackground(String... arg0) {
            try{

                URL url = new URL("https://lifeofshorties.eu/test/analytics.php");

                JSONObject postDataParams = new JSONObject();
                postDataParams.put("appCode", "236sseOJ82aad");

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                }
                else {
                    return new String("" + responseCode);
                }
            }
            catch(Exception e){
                return new String("Exception: " + e.getMessage());
            }

        }

        @Override
        protected void onPostExecute(String result) {
            parseAnalysis(result);
        }
    }

    private void parseAnalysis(String result) {
        try {
            JSONArray arr = new JSONObject(result).getJSONArray("data");

            for (int i=0; i < arr.length(); i++) {
                JSONObject temp = arr.getJSONObject(i);
                avgAge = temp.getString("avgAge");
                median = temp.getString("median");
                maxSalary = temp.getString("maxSalary");
                ratio = Float.parseFloat(temp.getString("maleRatio"));
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

package zcp.testing.EmployesViews;

import android.content.Context;
import android.os.AsyncTask;

import androidx.fragment.app.FragmentTransaction;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;

import zcp.testing.EmployesViews.Fragments.AddEmploFragment;
import zcp.testing.EmployesViews.Fragments.EmployeesFragment;
import zcp.testing.EmployesViews.Fragments.GoogleHitsFragment;

public class HTMLHelper extends AsyncTask<String, Void, String> {
    private Context context;

    public HTMLHelper(Context context) {
        this.context = context;
    }

    @Override
    protected String doInBackground(String... strings) {
        StringBuilder builder = new StringBuilder();
        try {
            Document doc = Jsoup.connect("https://www.google.com/search?q="+ strings[0]).get();
            Elements elements = doc.select("span[class=CVA68e qXLe6d]");
            if(elements.size() == 0)
                elements = doc.select("div[class=MUxGbd v0nnCb]");

            for(int i = 0; i < 5 && elements.size() >= 5; i++) {
                builder.append(elements.get(i).text()+ "\n \n");
            }

        } catch (IOException e) {
            return new String("Exception: " + e.getMessage());
        }


        return builder.toString();
    }

    @Override
    protected void onPostExecute(String result) {
        ((DataClass)((MainActivity)context).getApplication()).setHeaders(result);

        FragmentTransaction ft = ((MainActivity)context).getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_holder, new GoogleHitsFragment());
        ft.commit();
    }
}

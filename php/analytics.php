<?php
require 'db_connect.php';

$response = array();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    
    $appCode = !empty($_POST['appCode']) ? trim($_POST['appCode']) : null;
    
	if($appCode === '236sseOJ82aad')
	{
		$sql = "SELECT MAX(salary) as maxSalary, 
		(SELECT avg(YEAR(CURDATE()) - YEAR(birth_date)) FROM employees) as avgAge, 
		(SELECT sum(case when `Gender` = 'female' then 1 else 0 end)/count(*) FROM employees) as maleRatio, 
		(SELECT AVG(dd.age) 
			FROM ( SELECT YEAR(CURDATE()) - YEAR(birth_date) as age, @rownum:=@rownum+1 as `row_number`, @total_rows:=@rownum 
					FROM employees, (SELECT @rownum:=0) r 
					ORDER BY age ) as dd WHERE dd.row_number IN 
		( FLOOR((@total_rows+1)/2), FLOOR((@total_rows+2)/2) )) as median 
		FROM employees";
		$stmt = $pdo->prepare($sql);
	}
    $stmt->execute();
    
    $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    
    if($data === false){
        $response['error'] = true; 
        $response['message'] = 'No users';
    } else{		
        $response['data'] = $data;
    }
    
}
 
 echo json_encode($response);
?>
<?php
require 'db_connect.php';

$response = array();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    
    $name = !empty($_POST['name']) ? trim($_POST['name']) : null;
    $birthDay = !empty($_POST['birthDay']) ? trim($_POST['birthDay']) : null;
    $gender = !empty($_POST['gender']) ? trim($_POST['gender']) : null;
    $salary = !empty($_POST['salary']) ? trim($_POST['salary']) : null;
    $appCode = !empty($_POST['appCode']) ? trim($_POST['appCode']) : null;
	
	if($appCode === '236sseOJ82aad')
	{
		$sql = "INSERT INTO employees (name, birth_date, gender, salary) VALUES (?,?,?,?)";
		$stmt = $pdo->prepare($sql);
		
		$stmt->execute([$name, $birthDay, $gender, $salary]);
	}
    
    if($user === false){
        $response['error'] = true; 
        $response['message'] = 'Invalid response!';
    } else{		
        $response['user'] = $user;             
    }
    
}
 
 echo json_encode($response);
?>